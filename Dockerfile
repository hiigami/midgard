FROM python:3.6

RUN apt-get update && apt-get install -y \
    apt-transport-https \
    build-essential \
    apt-utils \
    ca-certificates \
    automake \
    bison \
    flex \
    g++ \
    libboost1.55-all-dev \
    libevent-dev \
    libssl-dev \
    libtool \
    make \
    pkg-config \
    curl \
    git \
    libgdal-dev \
    sudo \
    locales \
    --no-install-recommends && rm -rf /var/lib/apt/lists/*

RUN sed -i -e 's/# es_MX.UTF-8 UTF-8/es_MX.UTF-8 UTF-8/' /etc/locale.gen && \
    dpkg-reconfigure -f noninteractive locales
ENV LANG=es_MX.utf8

RUN mkdir -p /usr/src/vt
WORKDIR /usr/src/vt
COPY . /usr/src/vt

RUN pip install --upgrade pip && \
    pip install --no-cache-dir -r requirements.txt

CMD ["bash", "app.sh"]
