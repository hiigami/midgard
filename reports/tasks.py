from celery.decorators import periodic_task
from celery.task.schedules import crontab

from vt.compute import ComputeTask

from .models import Download, Latency, Upload

OPERATORS = [
    "internet.cierto.mx",
    "modem.iusacellgsm.mx",
    "wap.nexteldata.com.mx",
    "web.iusacellgsm.mx",
    "internet.itelcel.com",
    "internet.movistar.mx"
]

TECHNOLOGIES = [
    "HSPA+",
    "UMTS",
    "LTE",
    "HSPA",
    "UNKNOWN",
    "EDGE",
    "HSDPA",
    "HSUPA"
]

REPORT3_ITEMS = [
    {
        "barcode": "0000000000010",  # download
        "model": Download
    },
    {
        "barcode": "0000000000011",  # upload
        "model": Upload
    },
    {
        "barcode": "0000000000012",  # latency
        "model": Latency
    }
]


@periodic_task(
    run_every=(crontab(hour="23", minute="0", day_of_week="*")),
    ignore_result=True,
    options={
        "queue": 'report2'
    }
)
def report_2():
    compute_task = ComputeTask()
    for y in TECHNOLOGIES:
        for x in OPERATORS:
            compute_task.report('guadalajara', x, y)
            compute_task.report('cdmx', x, y)
            compute_task.report('mexico', x, y)


@periodic_task(
    run_every=(crontab(hour="02", minute="0", day_of_week="*")),
    ignore_result=True,
    options={
        "queue": 'report3'
    }
)
def report_3():
    compute_task = ComputeTask()
    for z in REPORT3_ITEMS:
        for y in TECHNOLOGIES:
            for x in OPERATORS:
                compute_task.report3('guadalajara', x, y, z[
                                     "barcode"], z["model"])
                compute_task.report3('cdmx', x, y, z["barcode"], z["model"])
                compute_task.report3('mexico', x, y, z["barcode"], z["model"])
