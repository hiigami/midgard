import decimal
from django.db import models
from products.models import City, Trait


class Merge(models.Model):
    city = models.ForeignKey(City, on_delete=models.DO_NOTHING)
    traits = models.ManyToManyField(Trait, blank=True)
    mu = models.DecimalField(max_digits=28, decimal_places=15)
    created = models.FloatField(db_index=True)

    def __str__(self):
        return self.city.name


class Download(models.Model):
    city = models.ForeignKey(City, on_delete=models.DO_NOTHING)
    traits = models.ManyToManyField(Trait, blank=True)
    mu = models.DecimalField(max_digits=28, decimal_places=15)
    created = models.FloatField(db_index=True)

    def __str__(self):
        return self.city.name


class Upload(models.Model):
    city = models.ForeignKey(City, on_delete=models.DO_NOTHING)
    traits = models.ManyToManyField(Trait, blank=True)
    mu = models.DecimalField(max_digits=28, decimal_places=15)
    created = models.FloatField(db_index=True)

    def __str__(self):
        return self.city.name


class Latency(models.Model):
    city = models.ForeignKey(City, on_delete=models.DO_NOTHING)
    traits = models.ManyToManyField(Trait, blank=True)
    mu = models.DecimalField(max_digits=28, decimal_places=15)
    created = models.FloatField(db_index=True)

    def __str__(self):
        return self.city.name
