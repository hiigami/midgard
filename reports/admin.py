from django.contrib import admin
from reports.models import Merge


class MergeAdmin(admin.ModelAdmin):

    list_display = ('city', 'mu', 'created')

admin.site.register(Merge, MergeAdmin)
