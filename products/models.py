import decimal
from django.db import models
from django.contrib.gis.db import models as models2


class Product(models.Model):
    name = models.CharField(max_length=20)
    isActive = models.BooleanField(default=True)
    created = models.DateTimeField(auto_now_add=True)

    class Meta:
        ordering = ('name',)

    def __str__(self):
        return self.name


class Attribute(models.Model):
    name = models.CharField(max_length=20)
    isActive = models.BooleanField(default=True)
    created = models.DateTimeField(auto_now_add=True)

    class Meta:
        ordering = ('name',)

    def __str__(self):
        return self.name


class Barcode(models.Model):
    product = models.ForeignKey(Product, on_delete=models.CASCADE)
    attributes = models.ManyToManyField(Attribute)
    code = models.CharField(db_index=True, max_length=13)
    isActive = models.BooleanField(default=True)
    created = models.DateTimeField(auto_now_add=True)

    class Meta:
        ordering = ('product', 'code',)

    def __str__(self):
        return self.code


class Trait(models.Model):
    attribute = models.ForeignKey(Attribute, on_delete=models.DO_NOTHING)
    description = models.CharField(max_length=30)

    class Meta:
        ordering = ('attribute', 'description',)

    def __str__(self):
        return self.description


class Result(models2.Model):
    barcode = models.ForeignKey(
        Barcode, db_index=True, on_delete=models.CASCADE)
    traits = models.ManyToManyField(Trait, blank=True)
    point = models2.PointField(blank=True)
    mu = models2.DecimalField(max_digits=28, decimal_places=15)
    prec_radius = models2.FloatField()
    sigma = models2.DecimalField(max_digits=28, decimal_places=15)
    altitude = models2.DecimalField(
        db_index=True, max_digits=28, decimal_places=15, default=0)
    altitude_accuracy = models2.DecimalField(max_digits=28,
                                             decimal_places=15,
                                             default=decimal.Decimal('1000000000000'))
    processed = models.BooleanField(db_index=True, default=False)
    created = models2.DateTimeField(auto_now_add=True)

    class Meta:
        ordering = ('barcode', '-created')

    def __str__(self):
        return self.barcode.__str__()


class City(models2.Model):
    name = models.CharField(max_length=20)
    shape = models2.GeometryField(blank=True)

    def __str__(self):
        return self.name
