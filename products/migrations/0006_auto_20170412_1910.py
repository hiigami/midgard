# -*- coding: utf-8 -*-
# Generated by Django 1.10.5 on 2017-04-13 00:10
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('products', '0005_auto_20170408_2039'),
    ]

    operations = [
        migrations.CreateModel(
            name='Trait',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('description', models.CharField(max_length=13)),
                ('attributes', models.ForeignKey(on_delete=django.db.models.deletion.DO_NOTHING, to='products.Attribute')),
            ],
            options={
                'ordering': ('attributes', 'description'),
            },
        ),
        migrations.AddField(
            model_name='result',
            name='traits',
            field=models.ManyToManyField(to='products.Trait'),
        ),
    ]
