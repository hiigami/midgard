# -*- coding: utf-8 -*-
# Generated by Django 1.10.5 on 2017-01-28 04:11
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('products', '0001_initial'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='result',
            options={'ordering': ('barcode', '-created')},
        ),
        migrations.AlterUniqueTogether(
            name='result',
            unique_together=set([]),
        ),
    ]
