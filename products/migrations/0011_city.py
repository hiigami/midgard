# -*- coding: utf-8 -*-
# Generated by Django 1.10.5 on 2017-09-25 01:04
from __future__ import unicode_literals

import django.contrib.gis.db.models.fields
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('products', '0010_auto_20170812_1810'),
    ]

    operations = [
        migrations.CreateModel(
            name='City',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=20)),
                ('shape', django.contrib.gis.db.models.fields.GeometryField(blank=True, srid=4326)),
            ],
        ),
    ]
