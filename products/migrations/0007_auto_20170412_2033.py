# -*- coding: utf-8 -*-
# Generated by Django 1.10.5 on 2017-04-13 01:33
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('products', '0006_auto_20170412_1910'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='trait',
            options={'ordering': ('attribute', 'description')},
        ),
        migrations.RenameField(
            model_name='trait',
            old_name='attributes',
            new_name='attribute',
        ),
    ]
