from django.contrib import admin
from products.models import Product, Attribute, Barcode, Result, Trait


class ProductAdmin(admin.ModelAdmin):

    list_display = ('name', 'isActive')


class AttributeAdmin(admin.ModelAdmin):

    list_display = ('name', 'isActive')


class BarcodeAdmin(admin.ModelAdmin):

    list_display = ('code', 'product', 'isActive')


class TraitAdmin(admin.ModelAdmin):

    list_display = ('attribute', 'description')


class ResultAdmin(admin.ModelAdmin):

    list_display = ('barcode', 'point', 'created', 'processed')


admin.site.register(Product, ProductAdmin)
admin.site.register(Attribute, AttributeAdmin)
admin.site.register(Barcode, BarcodeAdmin)
admin.site.register(Trait, TraitAdmin)
admin.site.register(Result, ResultAdmin)
