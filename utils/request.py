import requests
import json

class Request(object):
    __slots__ = []

    @classmethod
    def run(cls, url, method, data=None, content_type="application/json"):
        request = None
        if method == 'GET':
            request = requests.get
        else:
            request = requests.post
        try:
            req = request(url,
                          data=data,
                          headers={
                              "Content-Type": content_type
                              },
                          auth=("valuetrender", "B+s9t8O9Sm0U2GBw/YYZCg=="),
                          timeout=(3.05, 27)
                         )
            response = ""
            try:
                response = json.dumps(req.json())
            except:
                response = req.text
            return {'statusCode': req.status_code, 'res': response}
        except requests.exceptions.Timeout:
            raise Exception("Gateway Timeout")
        except:
            return {'statusCode': 500, 'res': 'Network is unreachable'}
