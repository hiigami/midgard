import decimal
import math

COLOR_MIN = {
    "red": 236,
    "green": 87,
    "blue": 47
}
COLOR_MAX = {
    "red": 68,
    "green": 227,
    "blue": 45
}

COLOR_LIMIT_MAX = math.pow(10, 12)
COLOR_LIMIT_MIN = COLOR_LIMIT_MAX * -1
COLORS = {
    "default": {
        "1": COLOR_MIN,
        "2": {
            "red": 223,
            "green": 128,
            "blue": 33
        },
        "3": {
            "red": 226,
            "green": 220,
            "blue": 55
        },
        "4": {
            "red": 165,
            "green": 230,
            "blue": 52
        },
        "5": COLOR_MAX
    },
    "temperature": {
        "1": {
            "red": 0,
            "green": 90,
            "blue": 255
        },
        "2": {
            "red": 0,
            "green": 255,
            "blue": 220
        },
        "3": {
            "red": 255,
            "green": 238,
            "blue": 0
        },
        "4": {
            "red": 255,
            "green": 139,
            "blue": 0
        },
        "5": {
            "red": 255,
            "green": 0,
            "blue": 115
        }
    }
}

MAX_ALPHA = decimal.Decimal(255)
HALF_ALPHA = decimal.Decimal(127)
HALF2_ALPHA = decimal.Decimal(64)
HALF3_ALPHA = decimal.Decimal(22)
HALF4_ALPHA = decimal.Decimal(11)
MIN_ALPHA = 1
MIN_ALLOWED = 0
MAX_ALLOWED = decimal.MAX_EMAX
HALF_ALLOWED = decimal.Decimal(10)
HALF2_ALLOWED = decimal.Decimal(100)
HALF3_ALLOWED = decimal.Decimal(1000)
HALF4_ALLOWED = decimal.Decimal(10000)


def safe_division(a, b):
    try:
        return a / b
    except:
        return 1

def get_speed_choice_0_through_5(mu):
    if mu == 0:
        return 1
    elif mu <= 20:
        return 2
    elif mu <= 40:
        return 3
    elif mu <= 60:
        return 4
    return 5

def get_signal_strength_choice_0_through_5(mu):
    if mu >= -30:
        return 5
    elif mu >= -67:
        return 4
    elif mu >= -70:
        return 3
    elif mu >= -80:
        return 2
    return 1

def get_latency_choice_0_through_5(mu):
    if mu >= 300:
        return 1
    elif mu >= 200:
        return 2
    elif mu >= 100:
        return 3
    elif mu >= 50:
        return 4
    return 5

def get_download_speed_choice_0_through_5(mu):
    if mu >= 25000:
        return 5
    elif mu >= 10000:
        return 4
    elif mu >= 5000:
        return 3
    elif mu >= 3000:
        return 2
    return 1

def get_upload_speed_choice_0_through_5(mu):
    if mu >= 4000:
        return 5
    elif mu >= 2000:
        return 4
    elif mu >= 500:
        return 3
    elif mu >= 300:
        return 2
    return 1

def get_absolute_color(mu, option = 0):
    choice = 1
    if option == 0:
        choice = math.ceil(get_choice_0_through_5(mu))
        if choice == 0:
            choice = 1
        return COLORS["default"][str(choice)]
    elif option == 1:
        choice = get_speed_choice_0_through_5(mu)
        return COLORS["temperature"][str(choice)]
    elif option == 3:
        choice = get_download_speed_choice_0_through_5(mu)
        return COLORS["temperature"][str(choice)]
    elif option == 4:
        choice = get_upload_speed_choice_0_through_5(mu)
        return COLORS["temperature"][str(choice)]
    elif option == 5:
        choice = get_latency_choice_0_through_5(mu)
        return COLORS["temperature"][str(choice)]
    choice = get_signal_strength_choice_0_through_5(mu)
    return COLORS["default"][str(choice)]

def get_relative_color(mu_min, mu_max, current):
    div = safe_division((current - mu_min), (mu_max - mu_min))
    p = ((1 - 0) * div + 0)
    red = int(COLOR_MIN["red"] + p * (COLOR_MAX["red"] - COLOR_MIN["red"]))
    green = int(COLOR_MIN["green"] + p *
                (COLOR_MAX["green"] - COLOR_MIN["green"]))
    blue = int(COLOR_MIN["blue"] + p * (COLOR_MAX["blue"] - COLOR_MIN["blue"]))
    return (red, green, blue)

def get_relative_color2(mu_min, mu_max, current, color1, color2):
    div = safe_division((current - mu_min), (mu_max - mu_min))
    p = ((1 - 0) * div + 0)
    red = int(color1["red"] + p * (color2["red"] - color1["red"]))
    green = int(color1["green"] + p * (color2["green"] - color1["green"]))
    blue = int(color1["blue"] + p * (color2["blue"] - color1["blue"]))
    return (red, green, blue)

def get_alpha(sigma):
    choice = get_sigma_choice_1_through_5(sigma)
    if choice == 5:
        return 255
    elif choice == 4:
        return 127
    elif choice == 3:
        return 64
    elif choice == 2:
        return 22
    elif choice == 1:
        return 11
    return 0

"""
def get_alpha(sigma):
    min_limit = MAX_ALLOWED
    max_limit = HALF4_ALLOWED
    alpha_max = HALF4_ALPHA
    alpha_min = MIN_ALPHA
    if sigma <= HALF_ALLOWED:
        # print('0')
        min_limit = HALF_ALLOWED
        max_limit = MIN_ALLOWED
        alpha_max = MAX_ALPHA
        alpha_min = HALF_ALPHA
    elif sigma <= HALF2_ALLOWED:
        # print('11')
        min_limit = HALF2_ALLOWED
        max_limit = HALF_ALLOWED
        alpha_max = HALF_ALPHA
        alpha_min = HALF2_ALPHA
    elif sigma <= HALF3_ALLOWED:
        # print('101')
        min_limit = HALF3_ALLOWED
        max_limit = HALF2_ALLOWED
        alpha_max = HALF2_ALPHA
        alpha_min = HALF3_ALPHA
    elif sigma <= HALF4_ALLOWED:
        # print('1001')
        min_limit = HALF4_ALLOWED
        max_limit = HALF3_ALLOWED
        alpha_max = HALF3_ALPHA
        alpha_min = HALF4_ALPHA
    return int((alpha_max - alpha_min) * (sigma - min_limit)
               / (max_limit - min_limit) + alpha_min)
"""


def get_choice_0_through_5(current, limit_min=COLOR_LIMIT_MIN, limit_max=COLOR_LIMIT_MAX):
    div = safe_division((current - limit_min), (limit_max - limit_min))
    num = round(5 * div, 2)
    if num > 5:
        return 5
    return num


def get_sigma_choice_1_through_5(sigma):
    if sigma <= 10:
        return 5
    elif sigma <= 100:
        return 4
    elif sigma <= 1000:
        return 3
    elif sigma <= 10000:
        return 2
    return 1

"""
w = 350
h = 350
color = 1
from PIL import Image
im = Image.new("RGB", (w, h))
pix = im.load()
for x in range(w):
    if (x + 1) % 71 == 0:
        color += 1
    RGBint = COLORS[str(color)]
    for y in range(h):
        pix[x,y] = (RGBint["red"], RGBint["green"], RGBint["blue"])
im.save("test.png", "PNG")
"""
"""
w = {
    "lat_array": [19.332208, 19.35],
    "long_array": [-99.191057, -99.1857],
    "time_array": [1484201087.671566, 0],
    "prec_radius_array": [3, 5],
    "mu_array": [7.999999999999999, 6],
    "sigma_array": [7.999999999999999, 8],
    "altitude_array": [10.0, 10.0],
    "altitude_accuracy_array": [10, 10],
    "traits_array": [
        [
            {"name": "Type", "description": "1"},
            {"name": "Type", "description": "2"}
        ]
    ],
    "arrays_exist": 1,
    "global": {"mu_sigma": [2.5, 1]},
    "color_array": [
        {"alpha": 255, "red": 226, "green": 220, "blue": 55},
        {"alpha": 255, "red": 226, "green": 0, "blue": 15}
    ]
}

w = 350
h = 350
import random
from PIL import Image
im = Image.new("RGB", (w, h))
pix = im.load()
for x in range(0, w):
    r,b,g = get_relative_color2(3, 5, 4, # min max avg
        {"alpha": 255, "red": 226, "green": 220, "blue": 55},
        {"alpha": 255, "red": 226, "green": 0, "blue": 15})
    for y in range(h):
        pix[x,y] = (r, g, b)
im.save("test.png", "PNG")
"""