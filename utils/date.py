import pytz
import time
from copy import deepcopy
from datetime import datetime, timedelta
from midgard.settings import TIME_ZONE

DEFAULT_TIME_ZONE = pytz.timezone(TIME_ZONE)


class DateHandler(object):
    __slots__ = []

    @staticmethod
    def today(timeZone=DEFAULT_TIME_ZONE):
        return datetime.now(tz=timeZone)

    @staticmethod
    def datetime_to_string(dt):
        return dt.isoformat()

    @staticmethod
    def add_sec_to_date(dt, sec):
        return deepcopy(dt + timedelta(seconds=sec))

    @staticmethod
    def now_timstamp(timeZone=DEFAULT_TIME_ZONE):
        return time.mktime(timeZone.localize(datetime.now()).timetuple())
