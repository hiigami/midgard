from abc import ABCMeta, abstractmethod
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status


class BaseView(APIView):
    __slots__ = [
        'serializer'
    ]
    __metaclass__ = ABCMeta

    def __init__(self, serializer):
        self.serializer = serializer

    def post(self, request, format=None):
        serializer = self.serializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
