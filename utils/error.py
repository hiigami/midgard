import sys
import traceback

def get_error(): 
    exc_type, exc_obj, tb = sys.exc_info()
    return "{0}-{1}-{2}".format(exc_type.__name__, exc_obj,
                                traceback.format_tb(tb))
