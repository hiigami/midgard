import os
import pickle
import logging
import threading
from enum import Enum
from random import choice
from functools import wraps
from utils.error import get_error
from utils.date import DateHandler


class State(Enum):
    Closed = 1
    HalfOpen = 2
    Open = 3


class CircuitBreaker(object):
    __slots__ = [
        "_lock",
        "_logger",
        "_file_path",
        "_name",
        "_last_state_change",
        "_time_out",
        "_state",
        "_fail_max",
        "_success_max",
        "_fail_counter",
        "_success_counter"
    ]

    def __init__(self, name, fail_max=5, time_out=60):
        self._lock = threading.RLock()
        self._logger = logging.getLogger(__name__)
        self._last_state_change = None
        self._file_path = None
        self._name = name
        self._time_out = time_out
        self._state = State.Closed
        self._fail_max = int(fail_max)
        self._success_max = int(fail_max)
        self._fail_counter = 0
        self._success_counter = 0

    def set_file_path(self, path):
        self._file_path = path

    def _save(self, path, obj):
        with open(path, "wb") as f:
            pickle.dump(obj, f)

    def _read(self, path):
        load = []
        if os.path.exists(path) and os.path.getsize(path) > 0:
            with open(path, "rb") as f:
                load = pickle.load(f)
        return load


    def _get_conf(self):
        path = "{0}/{1}.p".format(self._file_path,
                                  self._name
                                 )
        l = self._read(path)
        if len(l) > 0:
            self._time_out = l[0]
            self._state = l[1]
            self._fail_max = l[2]
            self._success_max = l[2]
            self._fail_counter = l[3]
            self._success_counter = l[4]
            self._last_state_change = l[5]

    def _save_conf(self):
        path = "{0}/{1}.p".format(self._file_path,
                                  self._name
                                 )
        l = [
            self._time_out,
            self._state,
            self._fail_max,
            self._fail_counter,
            self._success_counter,
            self._last_state_change
        ]
        self._save(path, l)

    def _trip(self):
        self._logger.warning(get_error())

    @property
    def state(self):
        return self._state

    def reset(self):
        self._fail_counter = 0
        self._success_counter = 0

    def _set_close_state(self):
        self._state = State.Closed
        self.reset()

    def _set_open_state(self):
        self._state = State.Open
        self._last_state_change = DateHandler.today()
        self.reset()

    def _run(self, fn, *args, **kwargs):
        r = False
        try:
            r = fn(*args, **kwargs)
        except:
            self._trip()
        self._logger.debug("Result was: %s", str(r))
        if r:
            self._success_counter += 1
            if self._success_counter >= self._success_max:
                self._set_close_state()
        else:
            self._fail_counter += 1
            if self._fail_counter >= self._fail_max:
                self._set_open_state()
            else:
                r = self._run(fn, *args, **kwargs)
        self._logger.debug("State set as: %s", str(self._state))
        return r

    def _half_open(self, fn, *args, **kwargs):
        r = False
        self._state = State.HalfOpen
        if choice([True, False]):
            r = self._run(fn, *args, **kwargs)
        return r

    def _open(self, fn, *args, **kwargs):
        r = False
        d = DateHandler.today()
        if DateHandler.add_sec_to_date(self._last_state_change,
                                       self._time_out
                                      ) < d:
            r = self._half_open(fn, *args, **kwargs)
        else:
            self._logger.info("State is still Open for: %s at: %s",
                              fn.__name__,
                              DateHandler.datetime_to_string(d)
                             )
        return r

    def call(self, fn, *args, **kwargs):
        r = False
        self._get_conf()
        if self._state == State.Open:
            r = self._open(fn, *args, **kwargs)
        elif self._state == State.HalfOpen:
            r = self._half_open(fn, *args, **kwargs)
        else:
            r = self._run(fn, *args, **kwargs)
        self._save_conf()
        return r

    def __call__(self, fn):
        @wraps(fn)
        def wrapper(*args, **kwargs):
            with self._lock:
                assert (self._file_path is not None), "No filePath was specified."
                return self.call(fn, *args, **kwargs)
        return wrapper
