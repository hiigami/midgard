python manage.py test

 ~/Downloads/redis-3.2.5/src/redis-server ~/Downloads/redis-3.2.5/redis.conf 
 
celery -A tasks worker -Q report2,celery --concurrency=1 -l debug -n worker1@%d & celery -A tasks worker -Q merge --concurrency=1 -l debug -n worker2@%d & celery -A tasks beat -l info 


python manage.py shell < start_tasks.py

docker build -t mistilteinn/midgard .
docker stop midgard
docker rm midgard
docker run -d --net=host --restart=always --name midgard -p 9091:9091 mistilteinn/midgard

--- tmp.py ---
from vt.tasks import *
compute_task_runner.apply_async()
--- tmp.py ---

celery -A tasks inspect active

"""
from vt.compute import ComputeTask
compute_task = ComputeTask()

compute_task.report('cdmx', 'internet.itelcel.com', 'LTE')
print(compute_task.run())

"""
