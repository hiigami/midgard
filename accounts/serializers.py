from rest_framework import serializers
from django.contrib.auth.models import User
from accounts.models import Phone


class SignUpSerializer(serializers.ModelSerializer):
    ID = serializers.SerializerMethodField('get_alternate_name')

    class Meta:
        model = User
        fields = ('ID', 'username', 'password', 'email')
        write_only_fields = ('password',)
        read_only_fields = ('ID',)

    def get_alternate_name(self, obj):
        return obj.id

    def create(self, validated_data):
        password = validated_data.pop("password")
        user = User.objects.create(**validated_data)
        if password:
            user.set_password(password)
            user.save()
        user.password = None
        return user


class AuthSerializerMixin(object):

    def restore_object(self, attrs, instance=None):
        if attrs.get("username", None):
            attrs["username"] = attrs["username"].lower()
        if attrs.get("email", None):
            attrs["email"] = attrs["email"].lower()
        if attrs.get("password", None):
            attrs["password"] = make_password(
                base64.decodestring(attrs["password"]))

        return super(AuthSerializerMixin, self).restore_object(attrs, instance)

    def validate_password(self, attrs, source):
        if len(attrs[source]) < 6:
            raise serializers.ValidationError(
                "Password must be at least 6 characters")
        return attrs


class LoginSerializer(AuthSerializerMixin, serializers.ModelSerializer):
    client_id = serializers.SerializerMethodField('get_client_id')
    client_secret = serializers.SerializerMethodField('get_client_secret')

    class Meta:
        model = User
        fields = ('client_id', 'client_secret')

    def get_client_id(self, obj):
        return obj.application_set.first().client_id

    def get_client_secret(self, obj):
        return obj.application_set.first().client_secret


class SocialSignUpSerializer(LoginSerializer):

    class Meta(LoginSerializer.Meta):
        fields = ('email', 'username', 'client_id', 'client_secret')
        read_only_fields = ('username',)


class PhoneSerializer(serializers.ModelSerializer):

    class Meta:
        model = Phone
        fields = '__all__'

    def create(self, validated_data, user):
        validated_data['user'] = user
        return Phone.objects.update_or_create(
            imei=validated_data['imei'],
            line=validated_data['line'],
            user=validated_data['user'],
            defaults=validated_data
        )
