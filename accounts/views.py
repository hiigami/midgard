from rest_framework import generics
from rest_framework import permissions
from accounts.permissions import IsAuthenticatedOrCreate
from accounts.serializers import SignUpSerializer, PhoneSerializer

from django.contrib.auth.models import User
from rest_framework import permissions

from utils.base_class_view import BaseView
from utils.json_response import JSONResponse


class SignUp(generics.CreateAPIView):
    queryset = User.objects.all()
    serializer_class = SignUpSerializer
    permission_classes = (IsAuthenticatedOrCreate,)


class Account(BaseView):
    __slots__ = [
        "permission_classes"
    ]

    def __init__(self):
        super(self.__class__, self).__init__(None)
        self.permission_classes = (permissions.IsAuthenticated,)

    def get(self, request, format=None):
        user = {
            "username": request.user.username,
            "email": request.user.email
        }
        return JSONResponse(user)

class Phones(BaseView):
    __slots__ = [
        "permission_classes"
    ]

    def __init__(self):
        super(self.__class__, self).__init__(PhoneSerializer)
        self.permission_classes = (permissions.IsAuthenticated,)

    def post(self, request, format=None):
        request.data["user"] = request.user.id
        serializer = self.serializer(data=request.data)
        if serializer.is_valid():
            serializer.create(serializer.data, request.user)
            return JSONResponse({}, status=201)
        return JSONResponse(serializer.errors, status=400)

from django.shortcuts import render
from oauth2_provider.models import AccessToken, Application, RefreshToken
from oauth2_provider.backends import get_oauthlib_core
from rest_framework import views
from utils.date import DateHandler
import datetime
from oauthlib.common import generate_token
from django.http import HttpResponseServerError


def index(request):
    social = None
    try:
        try:
            social = request.user.social_auth.get(provider='facebook')
        except:
            social = request.user.social_auth.get(provider='google-oauth2')
    except:
        return HttpResponseServerError()
    try:
        token = social.extra_data['access_token']
        aa = get_oauthlib_core()
        _app = Application.objects.get(name="vt")

        AccessToken.objects.filter(user=request.user,
                                   application=_app).delete()
        RefreshToken.objects.filter(user=request.user,
                                    application=_app).delete()
        _expires = datetime.datetime.fromtimestamp(
            DateHandler.now_timstamp()) + datetime.timedelta(seconds=36000)
        _access_token = AccessToken.objects.create(user=request.user,
                                                   token=generate_token(),
                                                   application=_app,
                                                   expires=_expires,
                                                   scope="read write"
                                                  )
        _refresh_token = RefreshToken.objects.create(user=request.user,
                                                     token=generate_token(),
                                                     application=_app,
                                                     access_token=_access_token)
        response = {
            "access_token": _access_token.token,
            "expires_in": 36000,
            "token_type": "Bearer",
            "scope": "read write",
            "refresh_token": _refresh_token.token
        }
        return JSONResponse(response)
    except:
        return HttpResponseServerError()
