from django.conf.urls import url, include
from rest_framework.urlpatterns import format_suffix_patterns
from accounts import views

urlpatterns = [
    url(r'^sign_up/$', views.SignUp.as_view()),
    url(r'^user/$', views.Account.as_view()),
    url(r'^oauth/', include('social_django.urls', namespace='social')),
    url(r'^profile/phone/', views.Phones.as_view()),
    url(r'^profile/', views.index),
]
