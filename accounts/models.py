from django.db import models
from django.contrib.auth.models import User

class Phone(models.Model):
    user = models.ForeignKey(User)
    imei = models.CharField(max_length=17)
    line = models.CharField(max_length=20)
    sim = models.CharField(max_length=30)
    updated = models.DateTimeField(auto_now=True)

    class Meta:
        ordering = ('user', '-updated')

    def __str__(self):
        return self.imei
