from django.contrib import admin
from accounts.models import Phone 


class PhoneAdmin(admin.ModelAdmin):

    list_display = ('user', 'imei', 'line', 'sim', 'updated')
    list_filter = ('user', 'line')

admin.site.register(Phone, PhoneAdmin)
