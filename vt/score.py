import json
import time
import decimal
import logging

from django.conf import settings
from django.contrib.auth.models import User

from utils.request import Request
from utils.error import get_error
from utils.date import DateHandler
from utils.circuit_breaker import CircuitBreaker

from products.models import Barcode
from vt.models import Score as ScoreModel

CB_SCORE = CircuitBreaker("Score")
CB_SCORE.set_file_path(settings.BASE_DIR)

class Score(object):

    def __init__(self):
        self._logger = logging.getLogger(__name__)

    def _get_user(self, user_id):
        try:
            return User.objects.get(id=user_id)
        except User.DoesNotExist:
            return None

    def get_barcode(self, code):
        try:
            if code == "-1":
                return Barcode.objects.all().distinct()
            return Barcode.objects.filter(code=code).first()
        except Barcode.DoesNotExist:
            return None

    def _get_from_db(self, user_id, barcode):
        try:
            """
            if barcode == "-1":
                return ScoreModel.objects \
                                 .order_by('barcode__code',
                                           'user',
                                           'points',
                                           'acquired') \
                                 .distinct('barcode__code').filter(user__id=user_id)
            """
            return ScoreModel.objects.filter(user__id=user_id, barcode=barcode)
        except ScoreModel.DoesNotExist:
            return []

    def _safe_division(self, a, b):
        if b == 0:
            if a == 0:
                return 0
            return 1
        try:
            return a / b
        except:
            return 1

    def _set_change_value(self, scores):
        q = scores.all()
        avg = 0
        num = 14
        if len(q) < num:
            num = len(q)
        if num > 0:
            num -= 1
        for x in range(num):
            avg += scores[x].points
        avg = self._safe_division(avg, num)
        if avg > 0:
            if scores[0].points < avg:
                return -1
            elif scores[0].points > avg:
                return 1
        return 0

    def get_score_object(self, user_id, barcode, get_model=False):
        _barcode = self.get_barcode(barcode)
        if barcode == "-1":
            all_scores = []
            for item in _barcode:
                user_score = self._get_from_db(user_id, item)
                all_scores.append(self._score_by_barcode(user_id, item, user_score, get_model))
            return all_scores
        user_score = self._get_from_db(user_id, _barcode)
        return self._score_by_barcode(user_id, _barcode, user_score, get_model)

    def _score_by_barcode(self, user_id, barcode, user_score, get_model=False):
        response = None
        user_obj = None
        actual_score = None
        try:
            if len(user_score) == 0:
                response = self.get(actual_score, 0)
                user_obj = self._get_user(user_id)
            else:
                user_obj = user_score[0].user
                actual_score = user_score[0]
                response = self.get(
                    actual_score.acquired, actual_score.points)
            if response is None or not response:
                response = {"score_update": 0, "change": 0, "barcode": barcode.code}
                if actual_score is not None:
                    response["score_update"] = float(actual_score.points)
                    response["change"] = self._set_change_value(user_score)
                    if get_model:
                        return actual_score
                else:
                    _score = ScoreModel(user=user_obj,
                                        barcode=barcode,
                                        points=decimal.Decimal(0),
                                        acquired=float(DateHandler.now_timstamp()))
                    _score.save()
                    if get_model:
                        return _score
            else:
                _score = ScoreModel(user=user_obj,
                                    barcode=barcode,
                                    points=decimal.Decimal(
                                        response["score_update"]),
                                    acquired=float(response["score_update_time"]))
                _score.save()
                if get_model:
                    return _score
                response["change"] = self._set_change_value(user_score)
            response["barcode"] = barcode.code
        except:
            self._logger.error(get_error())
        return response

    @CB_SCORE
    def get(self, score_time, score):
        if score_time is None:
            score_time = 0
        data = {
            "score_time": score_time,
            "score": float(score)
        }
        url = "http://{0}{1}".format(settings.VT_BASE,
                                     settings.VT_SCORE_PATH)
        response = Request.run(url,
                               "POST",
                               json.dumps(data))
        if response["statusCode"] == 200:
            return json.loads(response["res"])
        return None
