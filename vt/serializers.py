from rest_framework import serializers
from vt.score import Score
from vt.models import Score as ScoreModel


class ScoreSerializer(serializers.Serializer):
    barcode = serializers.CharField(max_length=13)


class LocationField(serializers.DictField):
    lat = serializers.DecimalField(max_digits=22, decimal_places=15)
    lng = serializers.DecimalField(max_digits=22, decimal_places=15)
    prec_radius = serializers.DecimalField(max_digits=22, decimal_places=15)
    altitude = serializers.DecimalField(
        max_digits=22, decimal_places=15, default=0)
    altitude_accuracy = serializers.DecimalField(
        max_digits=22, decimal_places=15, default=0)


class TraitField(serializers.DictField):
    name = serializers.CharField(max_length=20)
    description = serializers.CharField(max_length=30)


class ComputeSerializer(serializers.Serializer):
    id = serializers.IntegerField()
    quote = serializers.DecimalField(max_digits=22, decimal_places=9)
    barcode = serializers.CharField(max_length=13)
    location = LocationField()
    traits = serializers.ListField(child=TraitField(), default=[])


class ComputeQuerySerializer(serializers.Serializer):
    id = serializers.IntegerField()
    barcode = serializers.CharField(max_length=13)
    lat1 = serializers.DecimalField(max_digits=22, decimal_places=15)
    lng1 = serializers.DecimalField(max_digits=22, decimal_places=15)
    lat2 = serializers.DecimalField(max_digits=22, decimal_places=15)
    lng2 = serializers.DecimalField(max_digits=22, decimal_places=15)
    lat3 = serializers.DecimalField(max_digits=22, decimal_places=15)
    lng3 = serializers.DecimalField(max_digits=22, decimal_places=15)
    lat4 = serializers.DecimalField(max_digits=22, decimal_places=15)
    lng4 = serializers.DecimalField(max_digits=22, decimal_places=15)
    traits = serializers.ListField(child=TraitField(), default=[])
    realtime = serializers.IntegerField(min_value=0, max_value=1, default=1)
