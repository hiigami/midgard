from django.contrib import admin
from vt.models import Score, Compute


class ScoreAdmin(admin.ModelAdmin):

    list_display = ('user', 'barcode', 'points', 'acquired')
    list_filter = ('user', 'barcode')

class ComputeAdmin(admin.ModelAdmin):

    list_display = ('user', 'processed', 'created')
    list_filter = ('user', 'processed')


admin.site.register(Score, ScoreAdmin)
admin.site.register(Compute, ComputeAdmin)
