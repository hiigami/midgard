#from celery.task.schedules import crontab
#from celery.decorators import periodic_task
#from celery.utils.log import get_task_logger

from tasks import celery_app

from vt.compute import ComputeTask

compute_task = ComputeTask()
"""
@periodic_task(
    run_every=20,
    ignore_result=True
)
"""


@celery_app.task
def compute_task_runner():
    compute_task.run()

# compute_task_runner.apply_async()
