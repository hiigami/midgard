import mock
import decimal
import datetime
from django.test import TestCase
from rest_framework.test import APIRequestFactory
from django.core.urlresolvers import reverse
from django.utils import timezone
from django.contrib.auth.models import User
from django.contrib.gis.geos import Point
from oauth2_provider.models import AccessToken, Application
from oauthlib.common import generate_token

from utils.date import DateHandler

from vt.models import Score, Compute
from vt.views import ScoreView, ComputeView
from products.models import Product, Barcode, Attribute, Result, Trait
from vt.compute import ComputeTask

import json


class VTTest(TestCase):
    test_user = None
    test_user_token = None
    test_code = "1234567890987"
    barcode = None

    def create_user(self):
        password = 'mypassword'
        self.test_user = User.objects.create_superuser(
            'myuser', 'myemail@test.com', password)
        app = Application.objects.create(name="vt")
        expires = datetime.datetime.fromtimestamp(
            DateHandler.now_timstamp()) + datetime.timedelta(seconds=36000)
        self.test_user_token = AccessToken.objects.create(user=self.test_user,
                                                          token=generate_token(),
                                                          application=app,
                                                          expires=expires,
                                                          scope="read write")

    def create_product(self):
        product = Product.objects.create(name="Hapiness")
        self.barcode = Barcode.objects.create(
            product=product, code=self.test_code)

    def create_attribute(self):
        return Attribute.objects.create(name="Type")

    def create_score(self):
        score = Score.objects.create(user=self.test_user,
                                     barcode=self.barcode,
                                     points=2.443191379841373,
                                     acquired=DateHandler.now_timstamp())
    def create_trait(self, attribute, description):
        return Trait.objects.create(attribute=attribute, description=description)

    def create_result(self, traits = []):
        result = Result.objects.create(barcode=self.barcode,
                                       point=Point(float(19.332208),
                                                   float(-99.191057)),
                                       mu=decimal.Decimal("7.999999999999999"),
                                       prec_radius=2,
                                       sigma=decimal.Decimal(
                                           "985735253936.7985"),
                                       altitude=10,
                                       altitude_accuracy=12)
        for x in traits:
            result.traits.add(x)
        result.save()

    def create_compute(self, data):
        Compute.objects.create(user=self.test_user, data=json.dumps(data))

    def get_results_items(self):
        return Result.objects.all()

    def score_mock(self):
        return {
            'statusCode': 200,
            'res': json.dumps({
                "score_update": 2.443191379841373,
                "score_update_time": DateHandler.now_timstamp()
            })
        }

    def set_request(self, fn, url, data=None, format=None):
        request = fn(url, data=data, format=format, HTTP_AUTHORIZATION="Bearer {0}"
                     .format(self.test_user_token.token))
        request.user = self.test_user
        return request

    def setUp(self):
        self.create_user()
        self.create_product()
        self.factory = APIRequestFactory()

    @mock.patch('vt.score.Request.run')
    def test_1_get_score_no_token(self, mock_fn):
        mock_fn.return_value = self.score_mock()

        request = self.factory.get("/vt/score/?barcode=-1")
        request.user = self.test_user
        response = ScoreView.as_view()(request)
        self.assertEqual(response.status_code, 401)

    @mock.patch('vt.score.Request.run')
    def test_2_get_score_authenticated(self, mock_fn):
        mock_fn.return_value = self.score_mock()

        request = self.set_request(self.factory.get, "/vt/score/?barcode=-1")
        response = ScoreView.as_view()(request)
        self.assertEqual(response.status_code, 200)
        self.assertTrue("score_update" in str(response.content))

    @mock.patch('vt.score.Score.get')
    def test_3_add_compute_no_data(self, mock_fn):
        self.create_score()
        mock_fn.return_value = {
            "score_update": 2.443191379841373,
            "score_update_time": DateHandler.now_timstamp()
        }
        data = {
            "quote": 8,
            "barcode": self.test_code,
            "location": {
                "lat": 333,
                "lng": 444,
                "prec_radius": 3,
                "altitude_accuracy": 10,
                "altitude": 120
            }
        }
        request = self.set_request(
            self.factory.post, "/vt/compute/", data, "json")
        response = ComputeView.as_view()(request)
        self.assertEqual(response.status_code, 201)

    def test_4_get_compute_404(self):
        data = {
            "barcode": self.test_code,
            "lat1": 333,
            "lng1": 444,
            "lat2": 333,
            "lng2": 444,
            "lat3": 333,
            "lng3": 444,
            "lat4": 333,
            "lng4": 444
        }
        request = self.set_request(self.factory.get,
                                   "/vt/compute/",
                                   data)
        response = ComputeView.as_view()(request)
        self.assertEqual(response.status_code, 404)

    @mock.patch('vt.compute.Request.run')
    def test_5_get_compute_200(self, mock_fn):
        _attribute = self.create_attribute()
        _traits = []
        _traits.append(self.create_trait(_attribute, "1"))
        _traits.append(self.create_trait(_attribute, "2"))
        self.create_result(_traits)
        mock_fn.return_value = {
            'statusCode': 200,
            'res': json.dumps({
                "product": {
                    "barcode": self.barcode.code,
                    "product_id": self.barcode.product.id
                },
                "product_attributes": {
                    "location": {
                        "lat": 19.332208,
                        "long": -99.191057,
                        "prec_radius": 3,
                        "altitude_accuracy": 10,
                        "altitude": 120
                    },
                    "mu_sigma": [
                        7.999999999999999,
                        985735253936.7985
                    ],
                    "new_relevant_info": {
                        "new_sigma_array": [7.999999999999999],
                        "new_prec_radius_array": [3],
                        "new_altitude_accuracy_array": [10],
                        "new_altitude_accuracy_array": [10],
                        "new_common_time": [1484201087.671566]
                    }
                }
            })
        }
        data = {
            "barcode": self.test_code,
            "lat1": 19.336204,
            "lng1": -99.203874,
            "lat2": 19.336771,
            "lng2": -99.183875,
            "lat3": 19.322570,
            "lng3": -99.202301,
            "lat4": 19.325189,
            "lng4": -99.184104,
            "traits[0]": 1,
            "traits[1]": 2
        }
        request = self.set_request(self.factory.get,
                                   "/vt/compute/",
                                   data)
        response = ComputeView.as_view()(request)
        print(response.content)
        self.assertEqual(response.status_code, 200)

    def test_6_compute_task_none(self):
        compute_task = ComputeTask()
        result = compute_task._run(2)
        self.assertEqual(result, 12)
        results_items = self.get_results_items()
        self.assertEqual(len(results_items), 0)

    @mock.patch('vt.compute.Request.run')
    @mock.patch('vt.score.Score.get')
    def test_7_compute_task_same_traits(self, mock_fn, mock_fn2):
        mock_fn.return_value = json.loads(self.score_mock()["res"])
        mock_fn2.return_value = {
            'statusCode': 200,
            'res': json.dumps({
                "product": {
                    "barcode": self.barcode.code,
                    "product_id": self.barcode.product.id
                },
                "product_attributes": {
                    "location": {
                        "lat": 333,
                        "long": 444,
                        "prec_radius": 3,
                        "altitude": 10,
                        "altitude_accuracy": 12
                    },
                    "mu_sigma": [
                        7.999999999999999,
                        985735253936.7985
                    ],
                    "timestamp": 1484201087.671566
                },
                "user": {
                    "new_score": 2.4667663467513563,
                    "new_score_time": 1484200804.758541,
                    "uid": self.test_user.id
                }
            })
        }
        data = {
            'relevant_info': {},
            'user': {
                'superuser': 0,
                'score_time': 1485460903.275814,
                'location': {
                    'prec_radius': 20,
                    'lat': 19.332208,
                    'long': -99.191057,
                    "altitude": 10,
                    "altitude_accuracy": 20
                },
                'uid': self.test_user.id,
                'quote': 8,
                'score': '2.44319137984137313424071180634200572967529296875'
            },
            'product': {
                'product_id': self.barcode.product.id,
                'barcode': self.barcode.code,
                "traits": [{
                    "name": "Type",
                    "description": "2"
                    }]
            },
            'procedure': 1
        }
        _attribute = self.create_attribute()
        _traits = []
        _traits.append(self.create_trait(_attribute, "2"))
        self.create_result(_traits)
        self.create_compute(data)
        compute_task = ComputeTask()
        result = compute_task._run(2)
        self.assertEqual(result, 2)
        results_items = self.get_results_items()
        self.assertEqual(len(results_items), 2)
        self.assertTrue(results_items[1].processed)
        self.assertFalse(results_items[0].processed)

    @mock.patch('vt.compute.Request.run')
    @mock.patch('vt.score.Score.get')
    def test_8_compute_task_different_traits(self, mock_fn, mock_fn2):
        mock_fn.return_value = json.loads(self.score_mock()["res"])
        mock_fn2.return_value = {
            'statusCode': 200,
            'res': json.dumps({
                "product": {
                    "barcode": self.barcode.code,
                    "product_id": self.barcode.product.id
                },
                "product_attributes": {
                    "location": {
                        "lat": 333,
                        "long": 444,
                        "prec_radius": 3,
                        "altitude": 10,
                        "altitude_accuracy": 12
                    },
                    "mu_sigma": [
                        7.999999999999999,
                        985735253936.7985
                    ],
                    "timestamp": 1484201087.671566
                },
                "user": {
                    "new_score": 2.4667663467513563,
                    "new_score_time": 1484200804.758541,
                    "uid": self.test_user.id
                }
            })
        }
        data = {
            'relevant_info': {},
            'user': {
                'superuser': 0,
                'score_time': 1485460903.275814,
                'location': {
                    'prec_radius': 20,
                    'lat': 19.332208,
                    'long': -99.191057,
                    "altitude": 10,
                    "altitude_accuracy": 20
                },
                'uid': self.test_user.id,
                'quote': 8,
                'score': '2.44319137984137313424071180634200572967529296875'
            },
            'product': {
                'product_id': self.barcode.product.id,
                'barcode': self.barcode.code,
                "traits": [{
                    "name": "Type",
                    "description": "2"
                    }]
            },
            'procedure': 1
        }
        _attribute = self.create_attribute()
        _traits = []
        _traits.append(self.create_trait(_attribute, "1"))
        _traits.append(self.create_trait(_attribute, "2"))
        self.create_result(_traits)
        self.create_compute(data)
        compute_task = ComputeTask()
        result = compute_task._run(2)
        self.assertEqual(result, 2)
        results_items = self.get_results_items()
        self.assertEqual(len(results_items), 2)
        self.assertFalse(results_items[1].processed)
        self.assertFalse(results_items[0].processed)