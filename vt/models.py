from django.db import models
from django.contrib.postgres.fields import JSONField
from django.contrib.auth.models import User
from products.models import Barcode

class Score(models.Model):
    user = models.ForeignKey(User)
    barcode = models.ForeignKey(Barcode, db_index=True)
    points = models.DecimalField(max_digits=20, decimal_places=15)
    acquired = models.FloatField(db_index=True)

    class Meta:
        ordering = ('-acquired',)

    def __str__(self):
        return str(self.acquired)

class Compute(models.Model):
    user = models.ForeignKey(User)
    data = JSONField()
    processed = models.BooleanField(db_index=True, default=False)
    created = models.DateTimeField(auto_now_add=True)
    modified = models.DateTimeField(auto_now=True)

    class Meta:
        ordering = ('created',)
        permissions = (('is_supper_user', 'Can add content as superuser=1'),)

    def __str__(self):
        return str(self.created)
