from django.conf.urls import url
from rest_framework.urlpatterns import format_suffix_patterns
from vt import views

urlpatterns = [
    url(r'^score/$', views.ScoreView.as_view()),
    url(r'^compute/$', views.ComputeView.as_view()),
]
