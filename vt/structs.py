import decimal


class Base(object):
    __slots__ = []

    def _transform(self, obj):
        if isinstance(obj, decimal.Decimal):
            return str(obj)
        elif isinstance(obj, list):
            obj = [self._transform(x) for x in obj]
        return obj

    def __as_dict__(self):
        return {name: self._transform(getattr(self, name))
                for name in self.__slots__ if getattr(self, name) is not None}


class Product(Base):

    __slots__ = [
        "product_id",
        "barcode",
        "traits"
    ]

    def __init__(self,
                 product_id=None,
                 barcode=None,
                 traits=None):
        super(self.__class__, self).__init__()
        self.product_id = product_id
        self.barcode = barcode
        self.traits = traits


class Location(Base):

    __slots__ = [
        "lat",
        "long",
        "prec_radius",
        "altitude",
        "altitude_accuracy"
    ]

    def __init__(self,
                 lat=None,
                 lng=None,
                 prec_radius=None,
                 altitude=0,
                 altitude_accuracy=0):
        super(self.__class__, self).__init__()
        self.lat = lat
        self.long = lng
        self.prec_radius = prec_radius
        self.altitude = altitude
        self.altitude_accuracy = altitude_accuracy


class User(Base):

    __slots__ = [
        "quote",
        "location",
        "score_time",
        "score",
        "superuser",
        "uid"
    ]

    def __init__(self,
                 quote=None,
                 score_time=None,
                 score=None,
                 superuser=None,
                 uid=None,
                 location=None):
        super(self.__class__, self).__init__()
        self.quote = quote
        self.score = score
        self.superuser = superuser
        self.uid = uid
        self.score_time = score_time
        self.location = location.__as_dict__()


class RelevantInfo(Base):

    __slots__ = [
        "lat_array",
        "long_array",
        "time_array",
        "prec_radius_array",
        "mu_array",
        "sigma_array",
        "altitude_array",
        "altitude_accuracy_array",
        "traits_array",
        "arrays_exist"
    ]

    def __init__(self,
                 lat_array=None,
                 long_array=None,
                 time_array=None,
                 prec_radius_array=None,
                 mu_array=None,
                 sigma_array=None,
                 altitude_array=None,
                 altitude_accuracy_array=None,
                 traits_array=None,
                 arrays_exist=0):
        super(self.__class__, self).__init__()
        self.lat_array = lat_array
        self.long_array = long_array
        self.time_array = time_array
        self.prec_radius_array = prec_radius_array
        self.mu_array = mu_array
        self.sigma_array = sigma_array
        self.altitude_array = altitude_array
        self.altitude_accuracy_array = altitude_accuracy_array
        self.traits_array = traits_array
        self.arrays_exist = arrays_exist
