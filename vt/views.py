import re
import decimal

from django.contrib.gis.geos import Polygon, LinearRing
#from django.db.models import Q

from rest_framework import permissions

from products.models import Result

from vt.score import Score
from vt.compute import Compute
from vt.serializers import ComputeSerializer, ComputeQuerySerializer, ScoreSerializer

from utils.base_class_view import BaseView
from utils.json_response import JSONResponse
from utils.color_picker import get_absolute_color, get_alpha, get_speed_choice_0_through_5
from utils.color_picker import get_choice_0_through_5, get_sigma_choice_1_through_5


POINTREG = re.compile(r"(:?SRID=4326;POINT\s|\(|\))", re.IGNORECASE)


SCORE = Score()

from django.shortcuts import render
from django.views import View

class TermsAndConditions(View):
    def get(self, request):
        return render(request, 'terms.html')

class Landing(View):
    def get(self, request):
        return render(request, 'landing.html')

class ScoreView(BaseView):
    __slots__ = [
        "permission_classes"
    ]

    def __init__(self):
        super(self.__class__, self).__init__(ScoreSerializer)
        self.permission_classes = (permissions.IsAuthenticated,)

    def get(self, request, format=None):
        serializer = self.serializer(data=request.query_params)
        if serializer.is_valid():
            return JSONResponse(SCORE.get_score_object(int(request.user.id),
                                                       serializer.data["barcode"]
                                                      )
                               )
        return JSONResponse(serializer.errors, status=400)

class ComputeView(BaseView):
    __slots__ = [
        "_compute"
        "permission_classes"
    ]

    def __init__(self):
        super(self.__class__, self).__init__(ComputeSerializer)
        self.permission_classes = (permissions.IsAuthenticated,)
        self._compute = Compute()

    def get_linear_ring(self, p1, p2, p3, p4):
        try:
            return LinearRing(p1, p2, p3, p4, p1)
        except:
            return LinearRing()

    def to_float_tuple(self, lat, lng):
        return (float(lat), float(lng))

    def get_map_relevant_info(self, location, barcode):
        try:
            poly = Polygon(self.get_linear_ring(self.to_float_tuple(location["lat1"],
                                                                    location["lng1"]),
                                                self.to_float_tuple(location["lat2"],
                                                                    location["lng2"]),
                                                self.to_float_tuple(location["lat3"],
                                                                    location["lng3"]),
                                                self.to_float_tuple(location["lat4"],
                                                                    location["lng4"])
                                               )
                          )
            res = Result.objects.all().filter(point__within=poly,
                                              barcode=barcode,
                                              processed=False)
            if len(location["traits"]) > 0:
                for x in location["traits"]:
                    res = res.filter(traits__description=x["description"],
                                     traits__attribute__name=x["name"])
            #if barcode.code == "0000000000008":
            #    res = res.filter(~Q(traits__description__startswith="WPA"))
            return res
        except Result.DoesNotExist:
            raise None

    def get_lat_lng(self, point):
        coord = re.sub(POINTREG, "", str(point)).split(" ")
        return [float(coord[0]), float(coord[1])]

    def _format_traits(self, traits, barcode):
        try:
            els = [{"name": trait.attribute.name, "description": trait.description}
                    for trait in traits if trait.attribute.name not in ["BSSID", "Capabilities"]]
            if barcode == "0000000000008":
                if not any([True for trait in traits if trait.attribute.name == "Capabilities" and trait.description.startswith("WPA")]):
                    els.append({"name": "Network", "description": "Open"})
                elif len(els) > 0:
                    els.append({"name": "Network", "description": "Secure"})
            return els
        except:
            return []

    def transform_results_to_relevant_info(self, data, barcode):
        response = {
            "lat_array": [],
            "lng_array": [],
            "time_array": [],
            "prec_radius_array": [],
            "mu_array": [],
            "sigma_array": [],
            "altitude_array": [],
            "altitude_accuracy_array": [],
            "traits_array": []
        }
        for obj in data:
            lat_lng = self.get_lat_lng(obj.point)
            response["lat_array"].append(lat_lng[0])
            response["lng_array"].append(lat_lng[1])
            response["time_array"].append(obj.created.timestamp())
            response["prec_radius_array"].append(float(obj.prec_radius) * 2)
            response["mu_array"].append(float(obj.mu))
            response["sigma_array"].append(float(obj.sigma))
            response["altitude_array"].append(float(obj.altitude))
            response["altitude_accuracy_array"].append(float(obj.altitude_accuracy))
            response["traits_array"].append(self._format_traits(obj.traits.all(), barcode))
        return response

    def get(self, request):
        data = {"id": request.user.id}
        data.update(request.query_params.dict())
        data["traits"] = data.get("traits", [])
        if type(data["traits"]) == str:
            import json
            data["traits"] = json.loads(data["traits"])
        serializer = ComputeQuerySerializer(data=data)
        if serializer.is_valid():
            barcode = SCORE.get_barcode(serializer.data["barcode"])
            color_option = 0
            if serializer.data["barcode"] == "0000000000006":
                color_option = 1
            elif serializer.data["barcode"] == "0000000000008":
                color_option = 2
            elif serializer.data["barcode"] == "0000000000010":
                color_option = 3
            elif serializer.data["barcode"] == "0000000000011":
                color_option = 4
            elif serializer.data["barcode"] == "0000000000012":
                color_option = 5
            if barcode is None:
                return JSONResponse({'error': "Barcode does not exists."},
                                    status=400)
            product_info = self._compute.create_product(barcode.product_id,
                                                        barcode.code,
                                                        [])
            map_info = self.get_map_relevant_info(serializer.data, barcode)
            if map_info is None or len(map_info) == 0:
                return JSONResponse({'error': "No data exists."},
                                    status=404)
            map_transformed = self.transform_results_to_relevant_info(map_info, barcode.code)
            relevant_info = self._compute.create_relevant_info(map_transformed["lat_array"],
                                                               map_transformed[
                                                                   "lng_array"],
                                                               map_transformed[
                                                                   "time_array"],
                                                               map_transformed[
                                                                   "prec_radius_array"],
                                                               map_transformed[
                                                                   "mu_array"],
                                                               map_transformed[
                                                                   "sigma_array"],
                                                               map_transformed[
                                                                   "altitude_array"],
                                                               map_transformed[
                                                                   "altitude_accuracy_array"],
                                                               map_transformed["traits_array"],
                                                               1
                                                              )
            result = relevant_info.__as_dict__()

            result["global"] = {"mu_sigma": [1000000, 0]}
            merge_request = self._compute.query(product_info.__as_dict__(),
                                                relevant_info.__as_dict__())
            if merge_request is not None and merge_request is not False:
                sigma = get_sigma_choice_1_through_5(
                    merge_request["product_attributes"]["mu_sigma"][1])

                mu = get_choice_0_through_5(
                    merge_request["product_attributes"]["mu_sigma"][0])
                if color_option > 0:
                    mu = merge_request["product_attributes"]["mu_sigma"][0]

                result["global"]["mu_sigma"] = [mu, sigma]
                if serializer.data["realtime"] == 1:
                    result["sigma_array"] = merge_request["product_attributes"][
                        "new_relevant_info"]["new_sigma_array"]
                    result["prec_radius_array"] = merge_request["product_attributes"][
                        "new_relevant_info"]["new_prec_radius_array"]
                    result["altitude_accuracy_array"] = merge_request["product_attributes"][
                        "new_relevant_info"]["new_altitude_accuracy_array"]
                    result["time_array"] = merge_request["product_attributes"][
                        "new_relevant_info"]["new_common_time"]

            result["color_array"] = []
            for x in range(len(result["lat_array"])):
                _color = get_absolute_color(float(result["mu_array"][x]), color_option)
                result["color_array"].append({
                    "alpha": get_alpha(decimal.Decimal(result["sigma_array"][x])),
                    "red": _color["red"],
                    "green": _color["green"],
                    "blue": _color["blue"]
                })
            return JSONResponse(result, status=200)
        return JSONResponse(serializer.errors, status=400)

    def post(self, request, format=None):
        request.data["id"] = request.user.id
        serializer = self.serializer(data=request.data)
        if serializer.is_valid():
            if serializer.data["location"]["altitude_accuracy"] == 0:
                serializer.data["location"]["altitude_accuracy"] = 1000000000000
            location_info = self._compute \
                                .create_location(serializer.data["location"]["lat"],
                                                 serializer.data["location"]["lng"],
                                                 float(serializer.data["location"]["prec_radius"]),
                                                 float(serializer.data["location"]["altitude"]),
                                                 float(serializer.data["location"][
                                                     "altitude_accuracy"])
                                                )
            barcode = SCORE.get_barcode(serializer.data["barcode"])
            if barcode is None:
                return JSONResponse({'error': "Barcode does not exists."},
                                    status=400)
            score = SCORE.get_score_object(request.user.id, barcode.code, True)
            _is_super_user = 0
            if request.user.has_perm('vt.is_supper_user'):
                _is_super_user = 1
            user_info = self._compute.create_user(serializer.data["quote"],
                                                  float(score.points),
                                                  score.acquired,
                                                  _is_super_user,
                                                  request.user.id,
                                                  location_info)
            product_info = self._compute.create_product(barcode.product_id,
                                                        barcode.code,
                                                        serializer.data["traits"])
            self._compute.save_add(product_info.__as_dict__(),
                                   user_info.__as_dict__(),
                                   score.user)
            return JSONResponse({}, status=201)
        return JSONResponse(serializer.errors, status=400)
