import re
import json
import time
import decimal
import logging

from django.conf import settings
from django.contrib.gis.geos import Point
from django.contrib.gis.measure import Distance

import vt.structs
from vt.models import Compute as ComputeModel, Score as ScoreModel
from vt.score import Score as ScoreClass

from products.models import Result, Barcode, Trait, Attribute, City

from reports.models import Merge

from utils.request import Request
from utils.error import get_error
from utils.date import DateHandler
from utils.circuit_breaker import CircuitBreaker


POINTREG = re.compile(r"(:?SRID=4326;POINT\s|\(|\))",  re.IGNORECASE)

CB_COMPUTE_QUERY = CircuitBreaker("ComputeQuery")
CB_COMPUTE_QUERY.set_file_path(settings.BASE_DIR)

CB_COMPUTE_ADD = CircuitBreaker("ComputeAdd")
CB_COMPUTE_ADD.set_file_path(settings.BASE_DIR)

class Compute(object):

    def create_product(self,
                       product_id,
                       barcode,
                       traits=None):
        return vt.structs.Product(product_id,
                                  barcode,
                                  traits)

    def create_location(self,
                        lat,
                        lng,
                        prec_radius,
                        altitude,
                        altitude_accuracy):
        return vt.structs.Location(lat,
                                   lng,
                                   prec_radius,
                                   altitude,
                                   altitude_accuracy)

    def create_user(self,
                    quote,
                    score,
                    score_time,
                    superuser,
                    uid,
                    location):
        return vt.structs.User(quote,
                               score_time,
                               score,
                               superuser,
                               uid,
                               location)

    def create_relevant_info(self,
                             lat_array=None,
                             lng_array=None,
                             time_array=None,
                             prec_radius_array=None,
                             mu_array=None,
                             sigma_array=None,
                             altitude_array=None,
                             altitude_accuracy_array=None,
                             traits_array=None,
                             arrays_exist=0):
        return vt.structs.RelevantInfo(lat_array,
                                       lng_array,
                                       time_array,
                                       prec_radius_array,
                                       mu_array,
                                       sigma_array,
                                       altitude_array,
                                       altitude_accuracy_array,
                                       traits_array,
                                       arrays_exist)

    def _call_api(self, data, url_path):
        url = "http://{0}{1}".format(settings.VT_BASE,
                                     url_path
                                    )
        response = Request.run(url,
                               "POST",
                               json.dumps(data)
                              )
        if response["statusCode"] == 200:
            return json.loads(response["res"])
        return None

    @CB_COMPUTE_QUERY
    def query(self, product, relevant_info):
        data = {
            "product": product,
            "procedure": 0,
            "relevant_info": relevant_info
            }
        return self._call_api(data, settings.VT_COMPUTE_MERGE_PATH)

    def save_add(self, product, user_info, user):
        data = {
            "product": product,
            "user": user_info,
            "procedure": 1,
            "relevant_info": {}
        }
        ComputeModel.objects.create(user=user, data=json.dumps(data))

    @CB_COMPUTE_ADD
    def add(self, data):
        return self._call_api(data, settings.VT_COMPUTE_ADD_PATH)


class ComputeTask(object):
    __slots__ = [
        "_logger",
        "_compute"
    ]

    def __init__(self):
        self._logger = logging.getLogger(__name__)
        self._compute = Compute()

    def _get_item(self):
        try:
            return ComputeModel.objects.filter(processed=False).first()
        except ComputeModel.DoesNotExist:
            return None

    def _get_relevant_info(self,
                           point,
                           radius,
                           barcode,
                           altitude,
                           altitude_accuracy,
                           trait_ids
                          ):
        try:
            res = Result.objects.filter(point__distance_lt=(point,
                                                            Distance(m=(radius * 2))
                                                           ),
                                        processed=False,
                                        altitude__range=[
                                            altitude - altitude_accuracy,
                                            altitude + altitude_accuracy
                                            ],
                                        barcode__code=barcode
                                       )
            if len(trait_ids) > 0:
                res = res.filter(traits__in=trait_ids)
            return res
        except Result.DoesNotExist:
            return []

    def _get_barcode(self, code):
        try:
            return Barcode.objects.filter(code=code).first()
        except Barcode.DoesNotExist:
            return None

    def _get_attribute(self, name):
        try:
            return Attribute.objects.get(name=name)
        except Attribute.DoesNotExist:
            return None

    def _get_trait(self, data):
        _traits = []
        for trait in data:
            try:
                _traits.append(Trait.objects \
                                    .get(attribute__name=trait["name"],
                                         description=trait["description"]
                                        )
                              )
            except Trait.DoesNotExist:
                try:
                    _trait = Trait.objects \
                                  .create(attribute=self._get_attribute(trait["name"]),
                                          description=trait["description"])
                    _traits.append(_trait)
                except:
                    self._logger.warning(get_error())
        return _traits

    def get_lat_lng(self, point):
        coord = re.sub(POINTREG, "", str(point)).split(" ")
        return [float(coord[0]), float(coord[1])]

    def _format_traits(self, traits):
        return [{"name": trait.attribute.name, "description": trait.description}
                for trait in traits.all()]

    def transform_results_to_relevant_info(self, data, traits_ids):
        response = {
            "lat_array": [],
            "lng_array": [],
            "time_array": [],
            "prec_radius_array": [],
            "mu_array": [],
            "sigma_array": [],
            "altitude_array": [],
            "altitude_accuracy_array": [],
            "traits_array": []
        }
        to_remove = []
        for obj in data:

            tmp_ids = [x.id for x in obj.traits.all()]
            obj1 = set(traits_ids)
            obj2 = set(tmp_ids)
            if obj1 == obj2:
                lat_lng = self.get_lat_lng(obj.point)
                response["lat_array"].append(lat_lng[0])
                response["lng_array"].append(lat_lng[1])
                response["time_array"].append(obj.created.timestamp())
                response["prec_radius_array"].append(float(obj.prec_radius))
                response["mu_array"].append(float(obj.mu))
                response["sigma_array"].append(float(obj.sigma))
                response["altitude_array"].append(float(obj.altitude))
                response["altitude_accuracy_array"].append(float(obj.altitude_accuracy))
                response["traits_array"] = self._format_traits(obj.traits.all())
            else:
                to_remove.append(obj.id)
        return {"data": response, "exclude": to_remove}

    def transform_results_to_relevant_info2(self, data, traits_ids):
        response = {
            "lat_array": [],
            "lng_array": [],
            "time_array": [],
            "prec_radius_array": [],
            "mu_array": [],
            "sigma_array": [],
            "altitude_array": [],
            "altitude_accuracy_array": [],
            "traits_array": []
        }
        to_remove = []
        for obj in data:
            lat_lng = self.get_lat_lng(obj.point)
            response["lat_array"].append(lat_lng[0])
            response["lng_array"].append(lat_lng[1])
            response["time_array"].append(obj.created.timestamp())
            response["prec_radius_array"].append(float(obj.prec_radius))
            response["mu_array"].append(float(obj.mu))
            response["sigma_array"].append(float(obj.sigma))
            response["altitude_array"].append(float(obj.altitude))
            response["altitude_accuracy_array"].append(float(obj.altitude_accuracy))
            response["traits_array"] = self._format_traits(obj.traits.all())
        return {"data": response, "exclude": to_remove}

    def _get_sleep_time(self, last_time):
        if last_time >= 300:
            return 300
        return last_time + 10

    def _update_responses_true(self, result_ids):
        if len(result_ids) > 0:
            try:
                return Result.objects.filter(id__in=result_ids).update(processed=True)
            except:
                self._logger.warning(get_error())
                return 0
        return 1

    def _has_traits(self, product):
        traits_ids = []
        if "traits" in product:
            _traits = self._get_trait(product["traits"])
            for item in _traits:
                traits_ids.append(item.id)
        return traits_ids

    def _data_from_item(self, item):
        data = json.loads(item.data)
        user = data["user"]
        score_class = ScoreClass()
        updated_score = score_class.get_score_object(user["uid"],
                                                     data["product"]["barcode"],
                                                     True)
        data["user"]["score_time"] = updated_score.acquired
        data["user"]["score"] = float(updated_score.points)
        data["user"]["quote"] = int(float(user["quote"]))
        location = user["location"]
        point = Point(location["lat"], location["long"])

        _traits_ids = self._has_traits(data["product"])

        info = self._get_relevant_info(point,
                                       location["prec_radius"],
                                       data["product"]["barcode"],
                                       decimal.Decimal(location["altitude"]),
                                       decimal.Decimal(location["altitude_accuracy"]),
                                       _traits_ids)
        arrays_exist = 0
        info_transformed = self.transform_results_to_relevant_info(info, _traits_ids)
        if len(info_transformed["data"]["lat_array"]) > 0:
            arrays_exist = 1
        relevant_info = self._compute.create_relevant_info(info_transformed["data"]["lat_array"],
                                                           info_transformed[
                                                               "data"]["lng_array"],
                                                           info_transformed["data"][
                                                               "time_array"],
                                                           info_transformed["data"][
                                                               "prec_radius_array"],
                                                           info_transformed[
                                                               "data"]["mu_array"],
                                                           info_transformed["data"][
                                                               "sigma_array"],
                                                           info_transformed["data"][
                                                               "altitude_array"],
                                                           info_transformed["data"][
                                                               "altitude_accuracy_array"],
                                                           info_transformed["data"][
                                                               "traits_array"],
                                                           arrays_exist)
        data["relevant_info"] = relevant_info.__as_dict__()
        _barcode = self._get_barcode(data["product"]["barcode"])
        response_ids_to_update = [x.id for x in info
                                  if x.id not in info_transformed["exclude"]]
        return data, response_ids_to_update, _barcode

    def _process_response(self,
                          response,
                          item,
                          data,
                          response_ids_to_update,
                          barcode_item):
        if self._update_responses_true(response_ids_to_update) > 0:
            item.processed = True
            item.save()
            mu_sigma = response["product_attributes"]["mu_sigma"]
            product_attributes = response["product_attributes"]["location"]
            user = response["user"]

            if data["user"]["superuser"] == 0:
                acquired = float(user["new_score_time"])
                ScoreModel.objects.create(user=item.user,
                                          barcode=barcode_item,
                                          points=decimal.Decimal(user["new_score"]),
                                          acquired=acquired)
            new_point = Point(product_attributes["lat"],
                              product_attributes["long"])

            new_result = Result.objects.create(point=new_point,
                                               mu=decimal.Decimal(
                                                   mu_sigma[0]),
                                               sigma=decimal.Decimal(
                                                   mu_sigma[1]),
                                               prec_radius=product_attributes[
                                                   "prec_radius"],
                                               altitude=decimal.Decimal(
                                                   product_attributes["altitude"]),
                                               altitude_accuracy=decimal.Decimal(
                                                   product_attributes["altitude_accuracy"]),
                                               barcode=barcode_item)
            if "traits" in response["product"]:
                _traits = self._get_trait(response["product"]["traits"])
                for x in _traits:
                    new_result.traits.add(x)
                new_result.save()
        else:
            self._logger.error("Not saving correctly")

    def _run(self, sleep_time_seconds):
        item = self._get_item()
        if item is not None:
            try:
                data, response_ids_to_update, _barcode = self._data_from_item(item)
                response = self._compute.add(data)
                self._logger.info("Response %s", response)

                if response is not None and response is not False:
                    self._process_response(response,
                                           item,
                                           data,
                                           response_ids_to_update,
                                           _barcode)
                    sleep_time_seconds = 2
                else:
                    sleep_time_seconds = self._get_sleep_time(sleep_time_seconds)
            except:
                self._logger.error(get_error())
        else:
            sleep_time_seconds = self._get_sleep_time(sleep_time_seconds)
        return sleep_time_seconds

    def run(self):
        sleep_time_seconds = 2
        while True:
            self._logger.info("Seconds %d", sleep_time_seconds)
            sleep_time_seconds = self._run(sleep_time_seconds)
            time.sleep(sleep_time_seconds)

    def _get_city(self, city_name):
        try:
            return City.objects.get(name=city_name)
        except City.DoesNotExist:
            return None

    def _get_item2(self,
                   traits1_description,
                   traits2_description,
                   attribute1_name="MNO",
                   attribute2_name="Technology",
                   barcode_code="0000000000005"):
        try:
            res = Result.objects.filter(traits__description=traits1_description,
                                        traits__attribute__name=attribute1_name,
                                        processed=False,
                                        barcode__code=barcode_code)
            res.filter(traits__description=traits2_description,
                       traits__attribute__name=attribute2_name)
            return res
        except Result.DoesNotExist:
            return None

    def report(self, city_name, mno, tech):
        city = self._get_city(city_name)
        items = self._get_item2(mno, tech)
        info = [x for x in items
                if city.shape.contains(Point(x.point.y, x.point.x))]
        if len(info) > 0:
            arrays_exist = 0
            info_transformed = self.transform_results_to_relevant_info2(info, [])
            if len(info_transformed["data"]["lat_array"]) > 0:
                arrays_exist = 1
            relevant_info = self._compute.create_relevant_info(info_transformed[
                "data"]["lat_array"],
                                                               info_transformed[
                                                                   "data"]["lng_array"],
                                                               info_transformed[
                                                                   "data"]["time_array"],
                                                               info_transformed[
                                                                   "data"]["prec_radius_array"],
                                                               info_transformed[
                                                                   "data"]["mu_array"],
                                                               info_transformed[
                                                                   "data"]["sigma_array"],
                                                               info_transformed[
                                                                   "data"]["altitude_array"],
                                                               info_transformed[
                                                                   "data"][
                                                                       "altitude_accuracy_array"],
                                                               info_transformed[
                                                                   "data"]["traits_array"],
                                                               arrays_exist)
            product_info = self._compute.create_product(5, "0000000000005", [])
            #print(product_info.__as_dict__())
            #print(relevant_info.__as_dict__())
            merge_request = self._compute.query(product_info.__as_dict__(),
                                                relevant_info.__as_dict__())
            if merge_request is not None and merge_request is not False:
                report = Merge.objects.create(created=float(
                    merge_request["product_attributes"]["timestamp"]),
                                              mu=decimal.Decimal(merge_request[
                                                  "product_attributes"]["mu_sigma"][0]),
                                              city=city)
                if "traits_array" in merge_request["product_attributes"]['relevant_info']:
                    _traits = self._get_trait(merge_request[
                        "product_attributes"]['relevant_info']["traits_array"])
                    for item in _traits:
                        report.traits.add(item)
                    report.save()
                self._logger.warning(mno,
                                     tech,
                                     city_name,
                                     DateHandler.today(),
                                     float(merge_request["product_attributes"]["timestamp"])
                                    )

    def report3(self, city_name, mno, tech, barcode, Table):
        city = self._get_city(city_name)
        items = self._get_item2(traits1_description=mno,
                                traits2_description=tech,
                                barcode_code=barcode)
        info = [x for x in items
                if city.shape.contains(Point(x.point.y, x.point.x))]
        if len(info) > 0:
            arrays_exist = 0
            info_transformed = self.transform_results_to_relevant_info2(info, [])
            if len(info_transformed["data"]["lat_array"]) > 0:
                arrays_exist = 1
            relevant_info = self._compute.create_relevant_info(info_transformed[
                "data"]["lat_array"],
                                                               info_transformed[
                                                                   "data"]["lng_array"],
                                                               info_transformed[
                                                                   "data"]["time_array"],
                                                               info_transformed[
                                                                   "data"]["prec_radius_array"],
                                                               info_transformed[
                                                                   "data"]["mu_array"],
                                                               info_transformed[
                                                                   "data"]["sigma_array"],
                                                               info_transformed[
                                                                   "data"]["altitude_array"],
                                                               info_transformed[
                                                                   "data"][
                                                                       "altitude_accuracy_array"],
                                                               info_transformed[
                                                                   "data"]["traits_array"],
                                                               arrays_exist)
            product_info = self._compute.create_product(5, barcode, [])
            #print(product_info.__as_dict__())
            #print(relevant_info.__as_dict__())
            merge_request = self._compute.query(product_info.__as_dict__(),
                                                relevant_info.__as_dict__())
            if merge_request is not None and merge_request is not False:
                report = Table.objects.create(created=float(
                    merge_request["product_attributes"]["timestamp"]),
                                              mu=decimal.Decimal(merge_request[
                                                  "product_attributes"]["mu_sigma"][0]),
                                              city=city)
                if "traits_array" in merge_request["product_attributes"]['relevant_info']:
                    _traits = self._get_trait(merge_request[
                        "product_attributes"]['relevant_info']["traits_array"])
                    for item in _traits:
                        report.traits.add(item)
                    report.save()
                self._logger.warning(mno,
                                     tech,
                                     city_name,
                                     DateHandler.today(),
                                     float(merge_request["product_attributes"]["timestamp"]))
